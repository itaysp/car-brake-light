# CAR BRAKE LIGHT v3.1

This is basicly an ATTINY85 ws2812b controller. 
It get 12V trigger signal from the car brake system and then start flashing
leds on a ws2812b strip.

It gets 12V input from the car, and has a [step down](https://www.aliexpress.com/item/32787166862.html) to 5V for ATTINY85 and the ws2812b.


The ATTINY85 uses a micronucleus bootloader to load programs via USB  
[https://github.com/micronucleus/micronucleus](https://github.com/micronucleus/micronucleus)


## Changelog

* Moved the Step Down module to the back 
* Small changes in schematic
* Added 3D housing

![Board Front](images/board.PNG)

![Board Back](images/back.PNG)

